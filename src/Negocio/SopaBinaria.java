/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Bit;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Oscar Bayona y Brayan Albarracin
 */
public class SopaBinaria {

    private Bit mySopaBinaria[][];
    public Bit subrayados[][];
    public Bit Binario[];
    int canFilas;
    int nCol;

    public SopaBinaria() {
    }

    /**
     * Constructor que llena la matriz de Bits con un archivo de Excel
     *
     * @param rutaArchivoExcel ruta de un archivo de Excel con extension .xls
     * @throws FileNotFoundException
     * @throws IOException
     * @throws Exception
     */
    public SopaBinaria(String rutaArchivoExcel) throws FileNotFoundException, IOException, Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(rutaArchivoExcel));
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        this.canFilas = hoja.getLastRowNum() + 1;
        this.mySopaBinaria = new Bit[canFilas][];
        this.subrayados = new Bit[canFilas][];
        //System.out.println("Filas:"+canFilas);
        HSSFRow fila1 = hoja.getRow(0);
        nCol = fila1.getLastCellNum();
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol = filas.getLastCellNum();
            //validacion de que La matriz SopaBinaria siempre será cuadrada o rectangular
            if (cantCol != nCol) {
                throw new Exception("La matriz no es cuadrada o rectangular");
            }
            this.mySopaBinaria[i] = new Bit[cantCol];
            this.subrayados[i] = new Bit[cantCol];
            for (int j = 0; j < cantCol; j++) {
                //Obtiene la celda y su valor respectivo
                //double r=filas.getCell(i).getNumericCellValue();
                double valo = (filas.getCell(j).getNumericCellValue());
                int valor = (int) valo;
                boolean valoR = false;
                boolean falso = false;
                if (valor == 1) {
                    valoR = true;
                } else {
                    if (valor == 0) {
                        valoR = false;
                    }
                }
                Bit value = new Bit(valoR);
                Bit falsos = new Bit(falso);
                //String valor=filas.getCell(j).getStringCellValue();
                //System.out.print(valor+"\t");
                this.mySopaBinaria[i][j] = value;
                this.subrayados[i][j] = falsos;
            }
            //System.out.println();
        }
    }

    /**
     * Método que convierte un entero decimal dado en un arreglo de Bits
     *
     * @param decimal El parámetro decimal define el numero que se va a buscar en la matriz binaria
     */
    public void convertirDecimal(int decimal) {
        if (decimal < 0) {
            this.Binario = new Bit[0];

        }
        this.Binario = new Bit[0];
        //StringBuilder binario = new StringBuilder();
        while (decimal > 0) {
            int residuo = (int) (decimal % 2);
            decimal = decimal / 2;
            boolean valoR = false;
            if (residuo == 1) {
                valoR = true;
            } else {
                if (residuo == 0) {
                    valoR = false;
                }
            }
            Bit value = new Bit(valoR);
            addElementoInicio(value);
            // Insertar el dígito al inicio de la cadena
            //binario.insert(0, String.valueOf(residuo));
        }
        //return binario.toString();
    }
    //Metodo de soporte para el metodo convertirDecimal
    public void addElementoInicio(Bit x) {
        Bit aux[];
        //redimensionar el vector
        aux = new Bit[this.Binario.length + 1];
        aux[0] = x;
        for (int i = 0; i < this.Binario.length; i++) {

            aux[i + 1] = this.Binario[i];

        }
        this.Binario = aux;
    }
//    public void binario(String bin){
//        //String[] stringArray = bin.split(' ');
//        int n = bin.length();
//        this.Binario = new Bit[n];
//        for (int i=0;i<n;i++){
//            boolean valor = false;
//        }
//    }

    /**
     * Método que devuelve el numero de veces que aparece el número buscado horizontalmente
     * @return Cuantas veces se encontro horizontalmente
     */
    public int getCuantasVeces_Horizontal() {
        int horizontal = 0;
        for (int i = 0; i < this.mySopaBinaria.length; i++) {
            int k = 0;
            for (int j = 0; j < this.mySopaBinaria[i].length; j++) {
                k=0;
                boolean encontro=false;
                if (this.mySopaBinaria[i].length >= this.Binario.length+j){
                    for (int m = j; m < j+this.Binario.length && encontro!=true; m++){
                        if (this.mySopaBinaria[i][m].isValor() == this.Binario[k].isValor()) {
                            k++;
                            if (k >= this.Binario.length) {
                                horizontal++;
                                k = 0;
                                encontro=true;
                                //aca intento hacer un for en reversa para guardar las casillas que contienen la respuesta en this.subrayados
                                for (int x = m; x >= j+this.Binario.length;x--){
                                    boolean rt = true;
                                    Bit rtas = new Bit(rt);
                                    this.subrayados[i][x].setValor(true);
                                }
                               }
                        } else {
                            k = 0;
                        }
                    }
                } 
//                if (this.mySopaBinaria[i][j].isValor() == this.Binario[k].isValor()) {
//                    k++;
//                    if (k >= this.Binario.length) {
//                        horizontal++;
//                        k = 0;
//                    }
//                    //if(this.mySopaBinaria[i][j+1]!=null && this.Binario[k]!=null){
//
//                } else {
//                    k = 0;
//                }
            }
        }
        return horizontal;
    }

    /**
     * Método que devuelve el numero de veces que aparece el número buscado verticalmente
     * 
     * @return Retorna cuantas veces se encontro verticalmente
     */
    public int getCuantasVeces_Vertical() {
        int vertical = 0;
        for (int i = 0; i < this.mySopaBinaria[0].length; i++) {
            int k = 0;
            for (int j = 0; j < this.mySopaBinaria.length; j++) {
                k = 0;
                boolean encontro=false;
                if (this.mySopaBinaria.length >= this.Binario.length+j) {
                    for (int m = j; m < j+this.Binario.length && encontro!=true; m++) {
                        if (this.mySopaBinaria[m][i].isValor() == this.Binario[k].isValor()) {
                            k++;
                            if (k >= this.Binario.length) {
                                vertical++;
                                k = 0;
                                encontro=true;
                                //aca haria un for en reversa para guardar las casillas que contienen la respuesta
                            }
                            //if(this.mySopaBinaria[i][j+1]!=null && this.Binario[k]!=null){
                        } else {
                            k = 0;
                        }
                    }
                }
            }
        }
        return vertical;
    }
    /**
     * Método que devuelve el numero de veces que aparece el número buscado diagonalmente hacia abajo
     * @return 
     */
    public int getCuantasVeces_DiagonalDown() {
        int diagonalDown = 0;
        for (int i = 0; i < this.mySopaBinaria.length; i++) {
            int k = 0;
            for (int j = 0; j < this.mySopaBinaria[i].length; j++) {
                k = 0;
                if (this.mySopaBinaria[i][j].isValor() == this.Binario[k].isValor()) {
                    int n = i;
                    int m = j;
                    boolean encontro = false;
                    while (encontro != true && n < this.mySopaBinaria.length && m < this.mySopaBinaria[n].length && this.Binario[k] != null) {

                        if (this.mySopaBinaria[n][m].isValor() == this.Binario[k].isValor()) {
                            k++;

                            if (k >= this.Binario.length) {
                                diagonalDown++;
                                k = 0;
                                encontro = true;
                                //aca haria un for en reversa para guardar las casillas que contienen la respuesta
                            }
                        } else {
                            k = 0;
                        }

                        m++;
                        n++;

                    }

                    //if(this.mySopaBinaria[i][j+1]!=null && this.Binario[k]!=null){
                } else {
                    k = 0;
                }
            }
        }
        return diagonalDown;
    }
    /**
     * Método que devuelve el numero de veces que aparece el número buscado diagonalmente hacia arriba
     * @return 
     */
    public int getCuantasVeces_DiagonalUp() {
        int diagonalUp = 0;
        for (int i = 0; i < this.mySopaBinaria.length; i++) {
            int k = 0;
            for (int j = 0; j < this.mySopaBinaria[i].length; j++) {
                k = 0;
                if (this.mySopaBinaria[i][j].isValor() == this.Binario[k].isValor()) {
                    int n = i;
                    int m = j;
                    boolean encontro = false;
                    while (encontro != true && n >= 0 && n < this.mySopaBinaria.length && m < this.mySopaBinaria[n].length && this.Binario[k] != null) {

                        if (this.mySopaBinaria[n][m].isValor() == this.Binario[k].isValor()) {
                            k++;

                            if (k >= this.Binario.length) {
                                diagonalUp++;
                                encontro = true;
                                k = 0;
                                //aca haria un for en reversa para guardar las casillas que contienen la respuesta
                            }
                        } else {
                            k = 0;
                        }

                        m++;
                        n--;

                    }

                    //if(this.mySopaBinaria[i][j+1]!=null && this.Binario[k]!=null){
                } else {
                    k = 0;
                }
            }
        }
        return diagonalUp;
    }
    
    public String imprimir() {
        System.out.println("Imprimiendo mi matriz:");
        String msg = "";
        for (Bit vector[] : mySopaBinaria) {
            for (Bit dato : vector) {
                msg += dato.isValor() + "\t";
            }
            msg += "\n";
        }
        return msg;
    }
    public String imprimirRta() {
        System.out.println("Imprimiendo mi matriz:");
        String msg = "";
        for (Bit vector[] : subrayados) {
            for (Bit dato : vector) {
                msg += dato.isValor() + "\t";
            }
            msg += "\n";
        }
        return msg;
    }
    /**
     * Método que devuelve el decimal convertido en Bits en forma de numero binario
     * @return 
     */
    public String mostrarDecimal() {

        String msg = "";

        for (Bit dato : Binario) {
            String mg = "";
            if (dato.isValor() == true) {
                mg = "1";
            } else {
                mg = "0";
            }
            msg += mg + "";
        }

        return msg;
    }
    public void crearInforme_PDF() throws Exception
    {
        Document documento = new Document();
        FileOutputStream ficheroPdf = new FileOutputStream("src/Vista/ficheroSalida.pdf");
        PdfWriter.getInstance(documento, ficheroPdf);
        documento.open();
        Paragraph parrafo = new Paragraph();
        parrafo.add("Incidencias del binario en la sopa \n");
        documento.add(parrafo);
        //aca imprimiria this.sopaBinaria en decimal (0,1) y resaltaria las casillas True de this.subrayados
        documento.close();
    }
}
